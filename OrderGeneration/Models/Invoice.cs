﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrderGeneration.Models
{
    public class Invoice
    {
        public int InvoiceID { get; set; }
        public System.DateTime DateOfSale { get; set; }
        public int Total { get; set; }
    }
}