﻿using OrderGeneration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace OrderGeneration.Controllers
{
    public class ProductController : Controller
    {
        //
        // GET: /Product/

        public ActionResult Index()
        {
            IEnumerable<Product> productList;
            HttpResponseMessage response = GlobalVariables.webApiClient.GetAsync("product").Result;
            productList = response.Content.ReadAsAsync<IEnumerable<Product>>().Result;
            return View(productList);
        }

    }
}
