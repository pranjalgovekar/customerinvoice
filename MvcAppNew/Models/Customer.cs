﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcAppNew.Models
{
    public class Customer
    {
        public int Id { get; set; } 
        public string CustomerName { get; set; }
    }
}