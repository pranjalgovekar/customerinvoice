﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcAppNew.Models
{
    public class ViewModel
    {
        public DateTime DateOfSale { get; set; }
        public int Total { get; set; }
        public int InvoiceDetailsID { get; set; }
        public int ProductID { get; set; }
        public int Quanitity { get; set; }
        public int Price { get; set; }
    }
}