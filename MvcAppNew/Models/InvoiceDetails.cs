﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcAppNew.Models
{
    public class InvoiceDetails
    {
        public int InvoiceDetailID { get; set; }
        public int InvoiceID { get; set; }
        public int ProductID { get; set; }
        public int Quantity { get; set; }
    }
}