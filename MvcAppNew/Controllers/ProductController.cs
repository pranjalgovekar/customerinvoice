﻿using MvcAppNew.Models;
using MvcAppNew;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace MvcAppNew.Controllers
{
    public class ProductController : Controller
    {
        //
        // GET: /Product/

        public ActionResult Index()
        {
            IEnumerable<Product> productList;
            HttpResponseMessage response = GlobalVariables.webApiClient.GetAsync("products").Result;
            response.EnsureSuccessStatusCode();
            productList = response.Content.ReadAsAsync<IEnumerable<Product>>().Result;
            return View(productList);
        }

    }
}
