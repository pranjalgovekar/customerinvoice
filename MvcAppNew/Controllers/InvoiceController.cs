﻿using MvcAppNew;
using MvcAppNew.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace MvcApp.Controllers
{
    public class InvoiceController : Controller
    {
        //
        // GET: /Order/

        public ActionResult Index()
        {
            //List<Product> productList;
            //HttpResponseMessage response = GlobalVariables.webApiClient.GetAsync("product").Result;
            //response.EnsureSuccessStatusCode();
            //productList = response.Content.ReadAsAsync<List<Product>>().Result;
            //ViewBag.ProductList = new SelectList(productList, "ProductID", "ProductName");
            //ViewData["Product"] = productList;
            return View();
        }

        //
        // GET: /Order/Details/5

        public ActionResult Details(int id)
        {
            var invoice = new Invoice()
            {
                InvoiceID = id
            };
            return View(invoice);
        }

        //
        // GET: /Order/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Order/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Order/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Order/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Order/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Order/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
