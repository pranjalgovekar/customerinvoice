﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using MvcAppNew.Models;
using System.Data.SqlClient;
using System.Configuration;

namespace MvcAppNew.Controllers
{
    public class CustomersController : ApiController
    {
        SqlConnection cn;
        SqlCommand cmd;
        SqlDataReader dr;

        CustomersController()
        {
            cn = new SqlConnection(ConfigurationManager.ConnectionStrings["order"].ToString());
            cmd = new SqlCommand();
            cmd.Connection = cn;
        }
        private OrderEntities13 db = new OrderEntities13();

        // GET: api/Customers
        public IQueryable<Customer> GetCustomers()
        {
            return db.Customers;
        }

        // GET: api/Customers/5
        [ResponseType(typeof(Customer))]
        public IHttpActionResult GetCustomer(string id)
        {

            //Customer customer = db.Customers.Find(id);
            //if (customer == null)
            //{
            //    return NotFound();
            //}

            cmd.CommandText = "SELECT Id,CustomerName FROM Customer WHERE CustomerName='"+id+"'";
            cn.Open();
            dr = cmd.ExecuteReader();
            List<Customer> cust = new List<Customer>();

            Customer c=new Customer();
            dr.Read();
                c.Id = Convert.ToInt32(dr.GetValue(0));
                c.CustomerName = dr.GetValue(1).ToString();
                cust.Add(c);
                return Ok(cust);
        }

        // PUT: api/Customers/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCustomer(int id, Customer customer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != customer.Id)
            {
                return BadRequest();
            }

            db.Entry(customer).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CustomerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Customers
        [ResponseType(typeof(Customer))]
        public IHttpActionResult PostCustomer(Customer customer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Customers.Add(customer);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = customer.Id }, customer);
        }

        // DELETE: api/Customers/5
        [ResponseType(typeof(Customer))]
        public IHttpActionResult DeleteCustomer(int id)
        {
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return NotFound();
            }

            db.Customers.Remove(customer);
            db.SaveChanges();

            return Ok(customer);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CustomerExists(int id)
        {
            return db.Customers.Count(e => e.Id == id) > 0;
        }
    }
}