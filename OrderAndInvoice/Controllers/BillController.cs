﻿using MvcAppNew.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MvcAppNew.Controllers
{
    public class BillController : ApiController
    {
        SqlConnection cn;
        SqlCommand cmd;
        SqlDataReader dr;

        BillController()
        {
            cn = new SqlConnection(ConfigurationManager.ConnectionStrings["order"].ToString());
            cmd = new SqlCommand();
            cmd.Connection = cn;
        }
        // GET: api/Bill
        public IHttpActionResult Get()
        {
            cmd.CommandText = "SELECT p.ProductName,p.Price,ivd.Quantity,i.DateOfSale,ivd.Total,i.GrandTotal FROM dbo.Product p JOIN dbo.InvoiceDetails ivd ON p.ProductID=ivd.ProductID JOIN dbo.Invoice i ON i.InvoiceID = ivd.InvoiceID";
            cn.Open();
            dr = cmd.ExecuteReader();
            List<Bill> list = new List<Bill>();

            while (dr.Read())
            {
                Bill b1 = new Bill();
                b1.ProductName = dr.GetValue(0).ToString();
                b1.Price = Convert.ToInt32(dr.GetValue(1));
                b1.Quantity = Convert.ToInt32(dr.GetValue(2));
                b1.DateOfSale = Convert.ToDateTime(dr.GetValue(3));
                b1.Total = Convert.ToInt32(dr.GetValue(4));
                b1.GrandTotal = Convert.ToInt32(dr.GetValue(5));
                list.Add(b1);
            }

            return Ok(list);
        }

        // GET: api/Bill/5
        public IHttpActionResult Get(int id)
        {
            cmd.CommandText = "SELECT p.ProductName,p.Price,ivd.Quantity,i.DateOfSale,ivd.Total,i.GrandTotal FROM dbo.Product p JOIN dbo.InvoiceDetails ivd ON p.ProductID=ivd.ProductID JOIN dbo.Invoice i ON i.InvoiceID = ivd.InvoiceID WHERE ivd.InvoiceID=" + id;
            cn.Open();
            dr = cmd.ExecuteReader();
            List<Bill> list = new List<Bill>();

            while (dr.Read())
            {
                Bill b1 = new Bill();
                b1.ProductName = dr.GetValue(0).ToString();
                b1.Price = Convert.ToInt32(dr.GetValue(1));
                b1.Quantity = Convert.ToInt32(dr.GetValue(2));
                b1.DateOfSale = Convert.ToDateTime(dr.GetValue(3));
                b1.Total = Convert.ToInt32(dr.GetValue(4));
                b1.GrandTotal = Convert.ToInt32(dr.GetValue(5));
                list.Add(b1);
            }

            return Ok(list);
        }

        // POST: api/Bill
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Bill/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Bill/5
        public void Delete(int id)
        {
        }
    }
}
