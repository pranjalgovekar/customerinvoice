﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcAppNew.Models
{
    public class Bill
    {
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public int Price { get; set; }
        public DateTime DateOfSale { get; set; }
        public int Total { get; set; }
        public int GrandTotal { get; set; }
    }
}